#include "list_doubly.hpp"

list_doubly *firstobj = nullptr;
list_doubly *lastobj = nullptr;
string last_sortkey = "name";
char global_direction = '+';

void list_doubly::add_item()
{
    if (lastobj == nullptr)
    {
        prev = firstobj;
        next = nullptr;
        if (firstobj != nullptr)
        {
            firstobj->next = this;
            lastobj = this;
        }
        else
        {
            firstobj = this;
            lastobj = this;
        }
    }
    else
    {
        prev = lastobj;
        next = nullptr;
        lastobj->next = this;
        lastobj = this;
    }
}

void list_doubly::print_item()
{
    const int numWidth = 14;
    unsigned int strWidth = ( name.length() > 5 ? name.length() : 5 ); // If another string is present, ( name.length() > other.length() ? name.length() : other.length() ); if more use if / else if structure

    cout << left << setw(strWidth) << setfill(' ') << "Name";
    cout << left << setw(numWidth) << setfill(' ') << "Value";
    cout << left << setw(numWidth) << setfill(' ') << "Lot Nr." << endl;

    cout << left << setw(strWidth) << setfill(' ') << name;
    cout << right << setw(numWidth) << setfill(' ') << value;
    cout << right << setw(numWidth) << setfill(' ') << lot_number << endl;
}

list_doubly *list_doubly::remove_item()
{
    if (this == firstobj)
    {
        this->next->prev = nullptr;
        firstobj = this->next;
    }
    else if (this == lastobj)
    {
        this->prev->next = nullptr;
        lastobj = this->prev;
    }
    else
    {
        prev->next = this->next;
        next->prev = this->prev;
    }

    delete this;
    return lastobj;
}

char list_doubly::print_direction(bool change)          // change the direction of print_list (and write_file) or just get the current direction.
{
    if (change)
        global_direction = ( (global_direction == '+') ? '-' : '+' );

    return global_direction;
}

void list_doubly::print_list(char direction)
{
    const int numWidth = 14;
    unsigned int strWidth = 5;
    list_doubly *obj = firstobj;

    while (obj != nullptr) // Find longest name; maybe repeat for other strings as well
    {
        if (obj->name.length() > strWidth)
        {
            strWidth = obj->name.length(); // May need to be appended for more string attributes
            strWidth++;
        }
        obj = obj->next;
    }

    cout << "Current entries: " << endl;
    cout << left << setw(strWidth) << setfill(' ') << "Name";
    cout << left << setw(numWidth) << setfill(' ') << "Value";
    cout << left << setw(numWidth) << setfill(' ') << "Lot Nr." << endl;
    if (direction == '+')
    {
        obj = firstobj;
        while (obj != nullptr)
        {
            cout << left << setw(strWidth) << setfill(' ') << obj->name;
            cout << right << setw(numWidth) << setfill(' ') << obj->value;
            cout << right << setw(numWidth) << setfill(' ') << obj->lot_number << endl;
            obj = obj->next;
        }
    }
    else if (direction == '-')
    {
        obj = lastobj;
        while (obj != nullptr)
        {
            cout << left << setw(strWidth) << setfill(' ') << obj->name;
            cout << right << setw(numWidth) << setfill(' ') << obj->value;
            cout << right << setw(numWidth) << setfill(' ') << obj->lot_number << endl;
            obj = obj->prev;
        }
    }
    else
    {
        cout << "Invalid option entered. Please try again." << endl;
    }
}

bool list_doubly::order_achieved(string key, list_doubly *start = firstobj)
{
    if (key == "value")
    {
        while ((start->next != nullptr) && (start->value <= start->next->value))
        {
            start = start->next;
        }
    }
    else if (key == "lot_number")
    {
        while ((start->next != nullptr) && (start->lot_number <= start->next->lot_number))
        {
            start = start->next;
        }
    }
    else if (key == "name")
    {
        while ((start->next != nullptr) && (start->name.compare(start->next->name) <= 0))
        {
            start = start->next;
        }
    }
    if (start->next == nullptr)
        return true;
    else
        return false;
}

bool list_doubly::list_follower(list_doubly *start, list_doubly *follower, char direction) // Find out if follower can be reached by start in the direction specified by char:
{                                                                                          // '+': Forward (next), '-': Backward (prev)
    if (start == follower || start == nullptr)
    {
        return false;
    }
    
    list_doubly *looper = start;

    if (direction != '+' && direction != '-')
    {
        throw "Bad direction. Use \'+\' or \'-\'.";
    }

    if (looper != follower)
    {
        if (direction == '+')
        {
            if (looper->prev == follower)
            {
                return false;
            }
            while (looper != follower && looper != nullptr) // Can looper reach the follower before reaching lastobj?
            {
                looper = looper->next;
            }
        }
        else if (direction == '-')
        {
            if (looper->next == follower)
            {
                return false;
            }
            while (looper != follower && looper != nullptr) // Can looper reach the follower before reaching firstobj?
            {
                looper = looper->prev;
            }
        }
    }

    if (looper == follower) // Has looper reached follower?
    {
        return true;
    }
    else if (looper == nullptr) // Or has it gone past lastobj / firstobj ?
    {
        return false;
    }
    else
    {
        throw "Error: Looper didn't loop until the end.";
    }
}

list_doubly *list_doubly::div_list(list_doubly *left, list_doubly *right, string key) // QUICKSORT: Sort list around (assumed) pivot, return real pivot
{
    list_doubly *cursor_l = left;
    list_doubly *cursor_r = right->prev;

    if (key == "value")
    {
        while (list_follower(cursor_l, cursor_r, '+'))
        {
            while ((cursor_l->next != right) && (cursor_l->value < right->value)) // Find items bigger than or equal to pivot (right) on left side of pivot
            {
                cursor_l = cursor_l->next;
            }
            while ((cursor_r->prev != left) && (cursor_r->value >= right->value)) // Find items smaller than pivot (right) from the right
            {
                cursor_r = cursor_r->prev;
            }
            if (list_follower(cursor_l, cursor_r, '+'))
            {
                sort_switch(cursor_l, cursor_r);
            }
        }
        if (cursor_l->value > right->value)
        {
            sort_switch(cursor_l, right);
        }
        return cursor_l;
    }
    else if (key == "lot_number")
    {
        while (list_follower(cursor_l, cursor_r, '+'))
        {
            while ((cursor_l->next != right) && (cursor_l->lot_number < right->lot_number)) // Find items bigger than or equal to pivot (right) on left side of pivot
            {
                cursor_l = cursor_l->next;
            }
            while ((cursor_r->prev != left) && (cursor_r->lot_number >= right->lot_number)) // Find items smaller than pivot (right) from the right
            {
                cursor_r = cursor_r->prev;
            }
            if (list_follower(cursor_l, cursor_r, '+'))
            {
                sort_switch(cursor_l, cursor_r);
            }
        }
        if (cursor_l->lot_number > right->lot_number)
        {
            sort_switch(cursor_l, right);
        }
        return cursor_l;
    }
    else
        throw "Invalid Key.";
}

void list_doubly::sort_compare(list_doubly *first, list_doubly *last, string key) // QUICKSORT: Base algorithm for numbers, calling div_list()
{
    list_doubly *div = nullptr;

    if (key == "value")
    {
        if (list_follower(first,last,'+'))
        {
            div = div_list(first, last, key);
            sort_compare(first, div, key);
            sort_compare(div->next, last, key);
        }
    }
    else if (key == "lot_number")
    {
        if (list_follower(first,last,'+'))
        {
            div = div_list(first, last, key);
            sort_compare(first, div, key);
            sort_compare(div->next, last, key);
        }
    }
    else
    {
        throw "Invalid key.";
    }
    return;
}

list_doubly *list_doubly::div_liststr(list_doubly *left, list_doubly *right, string key) // QUICKSORT: Sort list around (assumed) pivot for strings, return real pivot
{
    list_doubly *cursor_l = left;
    list_doubly *cursor_r = right->prev;

    if (key == "name")
    {
        while (list_follower(cursor_l, cursor_r, '+'))
        {
            while ((cursor_l->next != right) && (cursor_l->name.compare(right->name) < 0)) // Find items with "smaller" (abc) letters in string regarding pivot (right) on left side of pivot, thus needing to be left of pivot
            {
                cursor_l = cursor_l->next;
            }
            while ((cursor_r->prev != left) && (cursor_r->name.compare(right->name) >= 0)) // Find items with "higher" (xyz) or equal letters in string than pivot (right) from the right, thus needing to be left of the pivot
            {
                cursor_r = cursor_r->prev;
            }
            if (list_follower(cursor_l, cursor_r, '+'))
            {
                sort_switch(cursor_l, cursor_r);
            }
        }
        if (cursor_l->name.compare(right->name) > 0)
        {
            sort_switch(cursor_l, right);
        }
        return cursor_l;
    }
    else
    {
        throw "Invalid key.";
    }
}

void list_doubly::sort_comparestr(list_doubly *first, list_doubly *last, string key) // QUICKSORT: Base algorithm for strings, calling div_liststr()
{
    list_doubly *div = nullptr;

    if (key == "name")
    {
        if (list_follower(first,last,'+'))
        {
            div = div_liststr(first, last, key);
            sort_comparestr(first, div, key);
            sort_comparestr(div->next, last, key);
        }
    }
    return;
}

void list_doubly::sort_switch(list_doubly *left, list_doubly *right) // QUICKSORT: Switch positions of left and right in list
{
    list_doubly *prevl = left->prev, *nextl = left->next; // Save original pointers
    list_doubly *prevr = right->prev, *nextr = right->next;

    if (prevl == nullptr)
    {
        firstobj = right; // (Start: ) right -> (...)
    }
    else
    {
        prevl->next = right; // now lleft -> right -> (...)
    }
    
    if (nextr == nullptr)
    {
        lastobj = left; // (...) <- left (End)
    }
    else
    {
        nextr->prev = left; // now left <- rright <- (...)
    }

    right->prev = prevl; // Assign values fitting to position (in case of switching a firstobj / lastobj, prevl or nextr are already nullptrs)
    left->next = nextr;
    
    if (nextl != right)         // Is left right next to right?
    {
        right->next = nextl;
        nextl->prev = right;        // Fill in the "middle" if not next to each other
    }
    else
    {
        right->next = left;
    }

    if (prevr != left)
    {
        left->prev = prevr;
        prevr->next = left;
    }
    else
    {
        left->prev = right;
    }
}

void list_doubly::sort_list(string key, bool verbose)
{
    if (verbose)
    {
        cout << "Sorting by " << key << "..." << endl;
    }

    while (!(order_achieved(key)))
    {
        if (key == "value" || key == "lot_number") // All number keys go here
        {
            sort_compare(firstobj, lastobj, key);
        }
        else if (key == "name") // All string keys go here
        {
            sort_comparestr(firstobj, lastobj, key);
        }
        else
            throw "Invalid key.";
    }
        if (verbose)
        {
            cout << "Done sorting." << endl;
            last_sortkey = key;
        }
    // }
}

/*void list_doubly::sort_list()
{
    sort_list(last_sortkey);
}*/

list_doubly *list_doubly::find_item(string key, float value) // Find a specific item with "key" = "value".
{
    list_doubly *item = nullptr;

    sort_list(); // TODO: Append sort_list with error message for wrong key

    if (key == "value") // "True" floats, like value, go here
    {
        if ((value - firstobj->value) > (lastobj->value - value))
        {
            item = firstobj;
            while (value != item->value)
            {
                if (item->next != nullptr)
                {
                    item = item->next;
                }
                else
                {
                    cout << "No item with this value found." << endl;
                    throw value;
                }
            }
        }
        else
        {
            item = lastobj;
            while (value != item->value)
            {
                if (item->prev != nullptr)
                {
                    item = item->prev;
                }
                else
                {
                    cout << "No item with this value found." << endl;
                    throw value;
                }
            }
        }
    }                             // Repeat for every float attribute
    else if (key == "lot_number") // Actual int attributes
    {
        // Convert float to int
        int value_i = static_cast<int>(value);

        if ((value_i - firstobj->lot_number) > (lastobj->lot_number - value_i))
        {
            item = firstobj;
            while (value_i != item->lot_number)
            {
                if (item->next != nullptr)
                {
                    item = item->next;
                }
                else
                {
                    cout << "No item with this value found." << endl;
                    throw value;
                }
            }
        }
        else
        {
            item = lastobj;
            while (value != item->lot_number)
            {
                if (item->prev != nullptr)
                {
                    item = item->prev;
                }
                else
                {
                    cout << "No item with this value found." << endl;
                    throw value;
                }
            }
        }
    } // Repeat for every int attribute
    else
    {
        cout << "Key is invalid." << endl;
        throw key;
    }

    cout << "Item found!" << endl;
    item->print_item();
    sort_list(last_sortkey, false);
    return item;
}

list_doubly *list_doubly::find_item(string key, string value) // Find a specific item with "key" = "value".
{
    list_doubly *item = nullptr;

    sort_list(key, false); // TODO: Append sort_list with error message for wrong key

    if (key == "name")
    {
        if ((value[0] - firstobj->name[0]) >= (lastobj->name[0] - value[0])) // Very crude, but works in most cases; may add deeper checking
        {                                                                    // TODO: Use comparing function instead? bool compare_strings(string 1, string 2)) ?
            item = firstobj;
            while (value != item->name)
            {
                if (item->next != nullptr)
                {
                    item = item->next;
                }
                else
                {
                    cout << "No item with this value found." << endl;
                    throw value;
                }
            }
        }
        else
        {
            item = lastobj;
            while (value != item->name)
            {
                if (item->prev != nullptr)
                {
                    item = item->prev;
                }
                else
                {
                    cout << "No item with this value found." << endl;
                    throw value;
                }
            }
        }
    }
    else
    {
        cout << "Key is invalid." << endl;
        throw key;
    }
    // append for more possible string attributes

    cout << "Item found!" << endl;
    item->print_item();
    sort_list(last_sortkey, false);
    return item;
}

void list_doubly::edit_lot()
{
    string lotbuf;

    cout << "Please enter new lot number: " << endl;
    getline(cin,lotbuf);

    edit_lot(stoi(lotbuf));
}

void list_doubly::edit_value()
{
    string valbuf;

    cout << "Please enter new value: " << endl;
    getline(cin,valbuf);

    edit_value(stof(valbuf));
}

void list_doubly::edit_name()
{
    string name;

    cout << "Please enter new name: " << endl;
    getline(cin,name);

    edit_name(name);
}

list_doubly *list_doubly::get_start(char start)
{
    return ( (start == '+') ? firstobj : lastobj );
}

list_doubly *list_doubly::export_object(string filepath, char direction, int max_attr)
{
    fstream exportfile (filepath, ios::out | ios::app);

    if ( exportfile.is_open() )
    {
        for (int attr = 0; attr < max_attr ; attr++)
        {
            switch(attr)
            {
                case 0:
                    exportfile << this->name << ",";    // Repeat for all components
                    break;
                case 1:
                    exportfile << this->value << ",";
                    break;
                case 2:
                    exportfile << this->lot_number;     // No comma for last entry!
                    break;
            }
        }
        exportfile << endl;        
    }
    else
    {
        cout << "Cannot create file." << endl;
        throw filepath;
    }
    exportfile.close();
    return ( (direction == '+') ? this->next : this->prev );   
}