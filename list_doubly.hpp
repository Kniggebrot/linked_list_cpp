#ifndef __LIST_DOUBLY_H
#define __LIST_DOUBLY_H

#include <iostream>
#include <iomanip>
#include <string>
#include <fstream>

using namespace std;

extern char global_direction;
extern string last_sortkey;

class list_doubly
{
    private:
    // Variables needed in structure go here; if inheriting, no need for these; please make sure to have access functions for find_item and sort_list!
    string name;
    float value;
    int lot_number;
    
    // Pointers needed for list
    list_doubly* prev;
    list_doubly* next;

    // Functions
    public:
    void add_item();
    void print_item();
    list_doubly *remove_item();

    list_doubly *find_item(string key, float value);
    list_doubly *find_item(string key, string value);

    // Access functions for example configuration; if inheriting not needed
    void edit_lot(int lot) { this->lot_number = lot; };
    void edit_value(float invalue) { this->value = invalue; };
    void edit_name(string inname) { this->name = inname; };

    void edit_lot();
    void edit_value();
    void edit_name();

    list_doubly *export_object(string filepath, char direction, int max_attr);
    
    bool order_achieved(string key, list_doubly *start);                 // In module: start has firstobj as default
    bool list_follower(list_doubly *start, list_doubly *follower, char direction);
    list_doubly *div_list(list_doubly *left, list_doubly *right,string key);
    list_doubly *div_liststr(list_doubly *left, list_doubly *right,string key);
    void sort_compare(list_doubly *first, list_doubly *last, string key);
    void sort_comparestr(list_doubly *first, list_doubly *last, string key);
    void sort_switch(list_doubly *left, list_doubly *right);
    void sort_list(string key = last_sortkey, bool verbose = false);                    // In module: key has last_sortkey as default
	//void sort_list();
    
    void print_list(char direction = global_direction);                               // In module: direction has global_direction as default
    char print_direction(bool change = true);

    list_doubly *get_start(char start);

    list_doubly(string inname, float invalue, int inlot_number)
    {
        name = inname;
        value = invalue;
        lot_number = inlot_number;

        add_item(); 
    }

    list_doubly()
    {
        string valuebuf;
        string lotbuf;

        //cin.ignore(2,'\n');
        cout << "Please enter name: ";
        getline(cin,name);
        cout << "Please enter value: ";
        getline(cin,valuebuf);
        cout << "Please enter lot number: ";
        getline(cin,lotbuf);

        value = stof(valuebuf);
        lot_number = stoi(lotbuf);

        add_item(); 
    }

};

#endif // __LIST_DOUBLY_H
